# https://github.com/Cisco-Talos/clamav
# https://docs.clamav.net/
FROM alpine:3.20.3

LABEL maintainer="Greg Myers <gmyers@gitlab.com>"
LABEL description="ClamAV antivirus CI job"
LABEL version="0.0.2"

RUN apk add --no-cache git clamav clamav-libunrar && \
    rm -rf /var/cache/apk/* && \
    freshclam --no-warnings

# Switch to non-root user
USER clamav

CMD  [ "clamscan" ]
