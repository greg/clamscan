# clamscan

GitLab CI job to scan repository for known malware using [ClamAV](https://www.clamav.net/about)

This CI/CD component runs a ClamAV scan on all files in a GitLab repository branch using the `clamscan` tool.

## Usage

To include this component in your GitLab CI/CD configuration, add the following to your `.gitlab-ci.yml` file:

```yaml
include:
  - component: gitlab.com/greg/clamscan/clamscan@0.0.2
```

## Details

### `clamscan` Job

The `clamscan` job is responsible for running the ClamAV scan on the repository files. It uses a custom Docker image (`registry.gitlab.com/greg/clamscan:main`) that includes ClamAV and its dependencies.

The job performs the following steps:

1. Updates the ClamAV virus database using `freshclam`.
2. Runs `clamscan` on all files in the repository, recursively scanning directories and archives.
3. Generates a `clamscan-report.txt` file containing the scan results.

The job is set to `allow_failure: true`, which means that if the ClamAV scan detects any infected files, the job will fail but will not cause the entire pipeline to fail.

### Artifacts

If the `clamscan` job fails (i.e., if infected files are detected), the `clamscan-report.txt` file will be stored as a job artifact. This allows you to review the scan results and take appropriate action.

### Sample output

```shell
/builds/user/project/eicar.com.txt: Eicar-Test-Signature FOUND
/builds/user/project/eicar.com.txt!(0): Eicar-Test-Signature FOUND
/builds/user/project/eicarcom2.zip: Eicar-Test-Signature FOUND
/builds/user/project/eicarcom2.zip!ZIP:eicar_com.zip!(2)ZIP:eicar.com: Eicar-Test-Signature FOUND
/builds/user/project/eicar_com.zip: Eicar-Test-Signature FOUND
/builds/user/project/eicar_com.zip!(1)ZIP:eicar.com: Eicar-Test-Signature FOUND
/builds/user/project/eicar.com: Eicar-Test-Signature FOUND
/builds/user/project/eicar.com!(0): Eicar-Test-Signature FOUND

----------- SCAN SUMMARY -----------
Known viruses: 6295218
Engine version: 0.100.3
Scanned directories: 1
Scanned files: 7
Infected files: 4
Data scanned: 0.00 MB
Data read: 0.00 MB (ratio 0.00:1)
Time: 48.769 sec (0 m 48 s)
```

Created as proof of concept for [gitlab-ee/issues/6721](https://gitlab.com/gitlab-org/gitlab-ee/issues/6721) and [gitlab-ce/issues/53560](https://gitlab.com/gitlab-org/gitlab-ce/issues/53560)

Malware test files from [eicar.org](https://www.eicar.org/)
